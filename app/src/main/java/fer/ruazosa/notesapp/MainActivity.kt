package fer.ruazosa.notesapp

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    lateinit  var viewModel: NotesViewModel
    lateinit  var notesAdapter: NotesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listOfNotesView.layoutManager = LinearLayoutManager(applicationContext)
        viewModel = ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(NotesViewModel::class.java)
        notesAdapter = NotesAdapter(viewModel)
        listOfNotesView.adapter = notesAdapter

        viewModel.initializeRepository(this.application)

        viewModel.notesList.observe(this, Observer {
            notesAdapter.notifyDataSetChanged()
        })

        floatingActionButton.setOnClickListener({
            val intent = Intent(this, ActivityNoteDetails::class.java)
            startActivity(intent)
        })


    }

    override fun onResume() {
        super.onResume()
        viewModel.getNotesRepository()
    }



}