package fer.ruazosa.notesapp

import androidx.room.*


@Dao
interface NoteDao {

    @Query("SELECT * FROM note")
    fun getNotes(): List<Note>

    @Insert
    fun insertNote(vararg note:Note)

    @Delete
    fun deleteNote(vararg note: Note)

    /*@Update
    fun updateNote(vararg note: Note)
    */

    @Transaction
    fun updateNote(newNote: Note, oldNote:Note) {
        insertNote(newNote)
        deleteNote(oldNote)
    }

}