package fer.ruazosa.notesapp


import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlin.coroutines.coroutineContext

class NotesAdapter(listOfNotesViewModel: NotesViewModel):
    RecyclerView.Adapter<NotesAdapter.ViewHolder>() {

    var listOfNotes: NotesViewModel = listOfNotesViewModel

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        var noteTitleTextView: TextView ?= null
        var noteDateTextView: TextView ?= null
        var deleteNoteButton: Button ?= null

        init {
            noteDateTextView = itemView.findViewById(R.id.noteDateTextView)
            noteTitleTextView = itemView.findViewById(R.id.noteTitleTextView)
            deleteNoteButton = itemView.findViewById(R.id.deleteNoteButton)


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val noteListElement = inflater.inflate(R.layout.note_list_element, parent, false)
        return ViewHolder(noteListElement)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (listOfNotes != null) {
            holder.noteTitleTextView?.text = listOfNotes.notesList.value!![position].noteTitle
            holder.noteDateTextView?.text = listOfNotes.notesList.value!![position].noteDate.toString()

            holder.deleteNoteButton?.setOnClickListener({
                listOfNotes.removeNoteFromRepository(listOfNotes.notesList.value!![position])
                val toast = Toast.makeText(it.context, "To succesfully delete note please open it and press back!", Toast.LENGTH_LONG)
                toast.show()
            })

            holder.itemView.setOnClickListener {v: View ->
                val intent = Intent(v.context, ActivityNoteDetails::class.java)
                val activity = holder.itemView.context as Activity
                listOfNotes.notesList.value!![position].currentNotePosition = position
                intent.putExtra("note", listOfNotes.notesList.value!![position])
                activity.startActivityForResult(intent, 1)
            }

        }
    }


    override fun getItemCount(): Int {
        if (listOfNotes != null) {
            return listOfNotes.notesList.value!!.count()
        }
        else {
            return 0
        }
    }
}