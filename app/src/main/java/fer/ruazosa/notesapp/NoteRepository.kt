package fer.ruazosa.notesapp

import android.app.Application

object NoteRepository {

    var noteDao: NoteDao ?= null
    var noteList: MutableList<Note> ?= null

    fun initializeRepository(application: Application) {
        val database = NotesRoomDatabase.getDatabase(application)
        noteDao = database?.notesDao()
    }

    fun insertNote(note: Note) {
        noteDao?.insertNote(note)
    }

    fun getAllNotes(): List<Note>? {
        return noteDao?.getNotes()
    }

    fun removeNote(note: Note) {
        noteDao?.deleteNote(note)
    }

    fun updateNote(newNote: Note, oldNote: Note) {
        noteDao?.updateNote(newNote, oldNote)
    }

}