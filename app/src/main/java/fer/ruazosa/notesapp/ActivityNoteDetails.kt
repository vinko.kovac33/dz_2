package fer.ruazosa.notesapp

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import java.util.*
import kotlinx.android.synthetic.main.activity_note_details.*

class ActivityNoteDetails : AppCompatActivity() {

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_details)
        val PARAMETER_EDIT_NOTE: String ?= "note"

        var viewModel = ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(NotesViewModel::class.java)

        var lastPosition: Int = -1
        val intent = intent
        val note = intent.getSerializableExtra(PARAMETER_EDIT_NOTE) as Note?
        val oldNote: Note? = note
        if (note!= null) {
            noteTitleEditText.setText(note.noteTitle)
            noteDescriptionEditText.setText(note.noteDescription)
            lastPosition = note.currentNotePosition
        }

        saveNoteButton.setOnClickListener({
            var note = Note()
            note.noteId = null
            note.noteTitle = noteTitleEditText.text.toString()
            note.noteDescription = noteDescriptionEditText.text.toString()
            note.noteDate = Date()
            if (lastPosition > -1) {
                note.currentNotePosition = lastPosition
                if (oldNote != null) {
                    viewModel.updateNoteInRepository(note, oldNote)
                }
                lastPosition = -1
            }
            else {
                viewModel.saveNoteToRepository(note)
            }
            finish()
        })
    }
}