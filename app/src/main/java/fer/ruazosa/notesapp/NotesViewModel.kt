package fer.ruazosa.notesapp

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*

class NotesViewModel: ViewModel() {

    var notesList: MutableLiveData<List<Note>> = MutableLiveData<List<Note>>()

    fun getNotesRepository() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val allNotes = NoteRepository.noteDao?.getNotes()
                withContext(Dispatchers.Main) {
                    notesList.value = allNotes
                }
            }
        }
    }

    fun saveNoteToRepository(note: Note) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                NoteRepository.insertNote(note)
            }
        }
    }

    fun removeNoteFromRepository(note: Note) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                NoteRepository.removeNote(note)
            }
        }
    }

    fun updateNoteInRepository(newNote: Note, oldNote: Note) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                NoteRepository.updateNote(newNote, oldNote)
            }
        }
    }

    fun initializeRepository(application: Application) {
        NoteRepository.initializeRepository(application)
    }

}