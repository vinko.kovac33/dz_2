package fer.ruazosa.notesapp

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable
import java.util.*

@Entity(tableName = "note")
class Note: Serializable {
    @PrimaryKey(autoGenerate = true)
    var noteId: Int ?= null
    @ColumnInfo(name = "note_title")
    var noteTitle: String ?= null
    @ColumnInfo(name = "note_description")
    var noteDescription: String ?=  null
    @ColumnInfo(name = "note_date")
    var noteDate: Date?= null
    @ColumnInfo(name = "current_note_position")
    var currentNotePosition: Int = -1
}